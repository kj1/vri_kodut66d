<?php


require 'db_connection.php';
require 'data_validation.php';
include 'session.php';


	$kasutajanimi = "";
	$parool = "";


  	if (empty($_POST["Kasutajanimi"])) {
   	$kasutajanimiErr = "*Kasutajanimi peab olema täidetud";
  	} else {
   	$kasutajanimi = test_input($_POST["Kasutajanimi"]);
 	}

  	if (empty($_POST["Parool"])) {
   	$paroolErr = "*Parool peab olema täidetud";
  	} else {
   	$parool = test_input($_POST["Parool"]);
  	}



	if (isset($kasutajanimiErr)) {
		include('sisselogimine.php');
		echo "<br>" . $kasutajanimiErr;
		die();
	}

	if (isset($paroolErr)) {
		include('sisselogimine.php');
		echo "<br>" . $paroolErr;
		die();
	}


?>